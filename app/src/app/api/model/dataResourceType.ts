import { VariableGridArray } from 'app/layout/table-editing-cell/table-editing-cell-configuration';

/**
 * Model of a DataResourceType.
 */
export class DataResourceType {
    public dataResourceTypeId?: string;
    public description?: string;

    public variableGridArray?: VariableGridArray;
    
  
  }
  