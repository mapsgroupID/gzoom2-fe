/**
 * Model of a DetailKPI.
 */
export class DetailKPI {

    public workEffortId?: string;
    public workEffortName?: string;
    public workEffortNameLang?: string;
    public accountCode?: string;
    public accountName?: string;
    public accountNameLang?: string;
    public description?: string;
    public descriptionLang?: string;
    public abbreviation?: string;
    public abbreviationLang?: string;
    public scAmount?: string;
    public stAmount?: string;
    public sc2Amount?: string;
    public st2Amount?: string;
    public scIconAmount?: string;
    public rvcIconContentId?: string;
    public drcObjectInfo?: string;
    public stIconAmount?: string;
    public rvtIconContentId?: string;
    public drtObjectInfo?: string;
    public sc2IconAmount?: string;
    public rvc2IconContentId?: string;
    public drc2ObjectInfo?: string;
    public st2IconAmount?: string;
    public rvt2IconContentId?: string;
    public drt2ObjectInfo?: string;
    public glFiscalTypeId?: string;
    public descFTId?: string;
    public m4Amount?: string;
    public m3Amount?: string;
    public m2Amount?: string;
    public m1Amount?: string;
    public isAmount?: string;
    public p1Amount?: string;
    public p2Amount?: string;
    public p3Amount?: string;
    public p4Amount?: string;
    public decimalScale?: number;
}
