/**
 * Model of Enumararion
 */
export class Enumeration {
  constructor(
    public enumId?: string,
    public enumCode?: string,
    public enumTypeId?: string,
    public description?: string,
    public descriptionLang?: string,
    public sequenceId?: string
  ) { }
}
