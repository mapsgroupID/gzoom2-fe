/**
 * Model of PartyGroup
 */
export class PartyGroup {
  partyId?: string;
  groupName?: string;
  groupNameLocal?: string;
  officeSiteName?: string;
  annualRevenue?: number;
  numEmployees?: number;
  comments?: string;
  logoImageUrl?: string;
}
