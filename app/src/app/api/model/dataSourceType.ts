import { VariableGridArray } from 'app/layout/table-editing-cell/table-editing-cell-configuration';

/**
 * Model of a DataSourceType.
 */
export class DataSourceType {
    public dataSourceTypeId?: string;
    public description?: string;

    public variableGridArray?: VariableGridArray;
    
  
  }
  