import { PartyGroup } from "./partyGroup";

/**
 * Model of UserLoginValidPartyRole
 */
export class UserLoginValidPartyRole {
  userLoginId?: string;
  partyId?: string;
  roleTypeId?: string;
  comments?: string;
  partyGroup?: PartyGroup;
}
