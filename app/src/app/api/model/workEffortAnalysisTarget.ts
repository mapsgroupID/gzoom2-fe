/**
 * Model of a WorkEffortAnalysis.
 */
 export class WorkEffortAnalysisTarget {
  
    public workEffortName?: string;
    public workEffortId?: string;
    public workEffortEtch?: string;
    public workEffortNameLang?: string;
    public scAmount?: number;
    public rvcIconContentId?: string;
    public drcObjectInfo?: string;
    public stAmount?: number;
    public rvtIconContentId?: string;
    public drtObjectInfo?: string;
    public sc2Amount?: number;
    public rvc2IconContentId?: string;
    public drc2ObjectInfo?: string;
    public st2Amount?: number;
    public rvt2IconContentId?: string;
    public drt2ObjectInfo?: string;
  }
  