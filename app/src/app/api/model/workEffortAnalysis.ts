/**
 * Model of a WorkEffortAnalysis.
 */
export class WorkEffortAnalysis {
  public workEffortAnalysisId?: string; 
  public description?: string;
  public workEffortId?: string;
  public comments?: string;
  public labelM4Prev?: string;
  public labelM3Prev?: string;
  public labelM2Prev?: string;
  public labelM1Prev?: string;
  public labelPrev?: string;
  public labelP1Prev?: string;
  public labelP2Prev?: string;
  public labelP3Prev?: string;
  public labelP4Prev?: string;
  public referenceDate?: Date;

}
