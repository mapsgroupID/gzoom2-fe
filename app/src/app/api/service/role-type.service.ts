import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ApiClientService } from '../../commons/service/client.service';

import { RoleType } from '../../view/role-type/role-type/role-type';

@Injectable()
export class RoleTypeService {

  constructor(private client: ApiClientService) { }

  roleTypes(): Observable<RoleType[]> {
    return this.client
      .get(`role-types`).pipe(
        map(json => json.results as RoleType[])
      );
  }

}
