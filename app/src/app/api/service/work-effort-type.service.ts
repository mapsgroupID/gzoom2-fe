import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ApiClientService } from '../../commons/service/client.service';

import { WorkEffortType } from '../../view/report-print/report';

@Injectable()
export class WorkEffortTypeService {

  constructor(private client: ApiClientService) { }

  workEffortTypes(workEffortTypeId: string): Observable<WorkEffortType[]> {
    return this.client
      .get(`work-effort-type/like/${workEffortTypeId}`).pipe(
        map(json => json.results as WorkEffortType[])
      );
  }

  workEffortTypesParametric(workEffortTypeId: string): Observable<WorkEffortType[]> {
    return this.client
      .get(`work-effort-type/parametric/${workEffortTypeId}`).pipe(
        map(json => json.results as WorkEffortType[])
      );
  }
}
