/**
 * Model of a StatusItem.
 */
export class StatusItem {
    constructor(public statusId?: string, public description?: string) { }
  }
  