/**
 * Model of a UomType.
 */
export class UomType {
  constructor(public uomTypeId?: string, public description?: string, public id?: string, public buttonDetails?: boolean) { }
}
