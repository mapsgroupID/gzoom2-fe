/**
 * Model of a WorkEffort.
 */
export class WorkEffort {
    constructor(public workEffortId?: string, 
      public workEffortName?: string, 
      public workEffortTypeId?: string,
      public sourceReferenceId?: string ) { }
  }
  