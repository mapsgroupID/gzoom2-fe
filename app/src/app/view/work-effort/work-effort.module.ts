import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorkEffortComponent } from './work-effort/work-effort.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [WorkEffortComponent]
})
export class WorkEffortModule { }
