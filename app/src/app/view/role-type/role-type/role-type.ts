/**
 * Model of a RoleType.
 */
export class RoleType {
    constructor(public roleTypeId?: string, public description?: string) { }
  }
  