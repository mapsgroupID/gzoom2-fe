import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoleTypeComponent } from './role-type/role-type.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [RoleTypeComponent]
})
export class RoleTypeModule { }
