/**
 * Default project constants.
 */
export class Constants {
  static readonly DATE_FMT = 'dd/MM/yyyy';
}
