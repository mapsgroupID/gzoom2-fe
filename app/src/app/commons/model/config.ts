/**
 * Application configuration properties.
 */
export class ApplicationConfig {
  name: string;
  version: string;
}
