export interface chartConfig {
  type?: string,
  labels?: string[],
  etch?: string[],
  rangeMaxName?: string,
  dataMax?: number[],
  dataMin?: number,
  dataCon?: number[],
  dataTar?: number[],
  dataSC?: number[],
  dataST?: number[],
  dataSC2?: number[],
  dataST2?: number []
  scoreEtch1?: string,
  scoreEtch2?: string,
  scoreEtch3?: string,
  scoreEtch4?: string,
  showEtchDescr?: string,


}
