export interface emoticonCard {
    text?: string,
    value?: number,
    iconContentId?: string,  
  }