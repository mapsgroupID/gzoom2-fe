import { ChangePasswordComponent } from "app/shared/change-password/change-password.component";
import { LoaderComponent } from "app/shared/loader/loader.component";
import { ReportDownloadComponent } from "app/shared/report-download/report-download.component";
import { BreadcrumbComponent } from "./breadcrumb/breadcrumb.component";
import { ButtonSlideMenuComponent } from "./button-slide-menu/button-slide-menu.component";
import { ContentComponent } from "./content/content.component";
import { FocusComponent } from "./focus/focus.component";
import { FooterComponent } from "./footer/footer.component";
import { HeaderComponent } from "./header/header.component";
import { FolderMenuComponent } from "./sidebar/folder-menu/folder-menu.component";
import { LeafMenuComponent } from "./sidebar/leaf-menu/leaf-menu.component";
import { SidebarComponent } from "./sidebar/sidebar.component";
import { ToolbarDataTableComponent } from './toolbar-data-table/toolbar-data-table.component';
import { ButtonComponent } from './button/button.component';
import { TableComponent } from './table/table.component';
import { GaugeComponent } from "./gauge/gauge.component";
import { ChartComponent } from "./chart/chart.component";
import { ProgressBarComponent } from "./progress-bar/progress-bar.component";
import { EmoticonCardComponent } from "./emoticon-card/emoticon-card.component";
import { TimesheetCalendarComponent } from '../view/timesheet/timesheet/timesheet-calendar/timesheet-calendar.component';
import { TableCalendarComponent } from "./table-calendar/table-calendar.component";
import { TableEditingCellComponent } from "./table-editing-cell/table-editing-cell.component";
import { InformationBarComponent } from "./information-bar/information-bar.component";
import { RadioButtonComponent } from "./radio-button/radio-button.component";


export const layoutComponents = [FocusComponent,
    ContentComponent,
    SidebarComponent,
    FooterComponent,
    HeaderComponent,
    LoaderComponent,
    FolderMenuComponent,
    LeafMenuComponent,
    ReportDownloadComponent,
    ChangePasswordComponent,
    BreadcrumbComponent,
    ButtonSlideMenuComponent,
    ToolbarDataTableComponent,
    ButtonComponent,
    TableComponent,
    GaugeComponent,
    ChartComponent,
    ProgressBarComponent,
    EmoticonCardComponent,
    TableCalendarComponent,
    TimesheetCalendarComponent,
    TableEditingCellComponent,
    InformationBarComponent,
    RadioButtonComponent
];