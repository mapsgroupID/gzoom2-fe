export interface gaugeConfig {
  gaugeLabel?: string,
  gaugeValue?: number,
  gaugeMax?: number,
  gaugeMin?: number,
  fromValueRed?: number,
  fromValueYellow?: number,
  fromValueGreen?: number,

}